# Organization

After creating a base file in Dotgrid, run `./org.sh` to sort files by type.

- `dot` are source .dot files
- `svg` are rendered .svg (unused)
- `mid` are rendered .png, drag these into Nodewerk
- `fin` are final -glow[n]-{color}.png files, export canvas here

# Old files

Most of the `fin/` files don't have color information, see colors.txt for decimal RGB values
